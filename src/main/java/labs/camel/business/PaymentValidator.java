package labs.camel.business;

import labs.camel.RouteConfig;
import labs.camel.business.model.InvalidPayment;
import labs.camel.generator.model.Payment;
import lombok.AllArgsConstructor;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

import static labs.camel.RouteConfig.VALID;


@Component
@AllArgsConstructor
public class PaymentValidator implements Processor {

    private Validator validator;

    @Override
    public void process(Exchange exchange) throws Exception {
        Payment payment = exchange.getIn().getBody(Payment.class);
        Set<ConstraintViolation<Payment>> violations = validator.validate(payment);

        boolean isValid = violations.isEmpty();
        exchange.getOut().setHeader(VALID, isValid);
        if (isValid)
            exchange.getOut().setBody(payment);
        else
            exchange.getOut().setBody(InvalidPayment.builder()
                    .payment(payment)
                    .violations(violations)
                    .build());

    }
}
