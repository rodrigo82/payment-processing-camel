package labs.camel.business;

import labs.camel.generator.model.Payment;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;


@Component
public class DefinePaymentRoute implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {
        Payment payment = exchange.getIn().getBody(Payment.class);

    }

}
