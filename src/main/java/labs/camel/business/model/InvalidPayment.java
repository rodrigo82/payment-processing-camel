package labs.camel.business.model;

import labs.camel.generator.model.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.ConstraintViolation;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class InvalidPayment {

    private Payment payment;
    private Set<ConstraintViolation<Payment>> violations;

}
