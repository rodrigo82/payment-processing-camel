package labs.camel;

import com.github.javafaker.Faker;
import labs.camel.generator.PaymentProducer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.validation.Validation;
import javax.validation.Validator;

import static org.springframework.boot.SpringApplication.run;

@SpringBootApplication
public class Main {

    public static void main(String[] args) throws Exception {
        ApplicationContext ctx = run(Main.class, args);
        new Thread(ctx.getBean(PaymentProducer.class)).run();
    }

    @Bean
    public Faker faker(){
        return new Faker();
    }

    @Bean
    public Validator validator(){
        return Validation.buildDefaultValidatorFactory()
                .getValidator();
    }

}
