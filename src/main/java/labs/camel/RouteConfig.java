package labs.camel;

import labs.camel.business.DefinePaymentRoute;
import labs.camel.business.PaymentValidator;
import lombok.AllArgsConstructor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class RouteConfig extends RouteBuilder {

    public static final String INPUT = "direct:input";
    public static final String VALIDATION_OUTPUT = "direct:validationOutput";
    public static final String PROCESS = "direct:process";
    public static final String VALID = "valid";

    public void configure() throws Exception {

        from(INPUT)
                .bean(PaymentValidator.class)
                .choice()
                    .when(header(VALID).isEqualTo(true))
                        .to(PROCESS)
                    .otherwise()
                        .to(VALIDATION_OUTPUT);

        from(VALIDATION_OUTPUT).log("error: ${body}");

        from(PROCESS).bean(DefinePaymentRoute.class).log("processed ${body}");

    }
}
