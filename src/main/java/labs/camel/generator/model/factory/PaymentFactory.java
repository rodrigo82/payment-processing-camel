package labs.camel.generator.model.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.Payment;
import labs.camel.generator.model.paymentype.factory.PaymentDataFactory;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;
import java.util.function.Supplier;

@Component
@AllArgsConstructor
public class PaymentFactory implements Supplier<Payment> {

    private final Faker faker;
    private PaymentDataFactory paymentDataFactory;
    private ProductListFactory productListFactory;

    public Payment get() {
        return Payment.builder()
                .invoiceId(UUID.randomUUID().toString())
                .orderId(UUID.randomUUID().toString())
                .paymentData(paymentDataFactory.get())
                .tax(tax())
                .products(productListFactory.get())
                .shipping(faker.number().randomDouble(2, 2, 20))
                .date(new Date())
                .build();
    }

    private double tax() {
        return faker.number().numberBetween(10, 50) / 10.0;
    }
}
