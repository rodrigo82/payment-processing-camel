package labs.camel.generator.model.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.Product;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

import static java.util.stream.Collectors.toList;
import static java.util.stream.IntStream.range;
import static org.apache.commons.lang3.RandomUtils.nextInt;

@Component
@AllArgsConstructor
public class ProductListFactory implements Supplier<List<Product>> {

    private Faker faker;

    @Override
    public List<Product> get() {
        return range(1, nextInt(1, 10)).mapToObj(i -> Product.builder()
                .description(faker.commerce().productName())
                .price(faker.number().randomDouble(2, 1, 1500))
                .id(UUID.randomUUID().toString())
                .build()).collect(toList());
    }
}
