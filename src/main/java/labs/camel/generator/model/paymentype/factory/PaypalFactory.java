package labs.camel.generator.model.paymentype.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.paymentype.PaymentData;
import labs.camel.generator.model.paymentype.Paypal;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@AllArgsConstructor
public class PaypalFactory implements Supplier<PaymentData> {

    private Faker faker;

    @Override
    public Paypal get() {
        return Paypal.builder()
                .email(faker.internet().emailAddress())
                .build();
    }
}
