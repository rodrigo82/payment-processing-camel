package labs.camel.generator.model.paymentype.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.paymentype.PaymentData;
import labs.camel.generator.model.paymentype.Transfer;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@AllArgsConstructor
public class TransferFactory implements Supplier<PaymentData> {

    private Faker faker;

    @Override
    public Transfer get() {
        return Transfer.builder()
                .iban(faker.finance().iban())
                .build();
    }
}
