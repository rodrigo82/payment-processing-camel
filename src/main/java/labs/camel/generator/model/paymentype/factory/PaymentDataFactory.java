package labs.camel.generator.model.paymentype.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.paymentype.PaymentData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
@AllArgsConstructor
public class PaymentDataFactory implements Supplier<PaymentData> {

    private Faker faker;
    private CardFactory cardFactory;
    private PaypalFactory paypalFactory;
    private TransferFactory transferFactory;

    @Override
    public PaymentData get() {
        return faker.options().option(cardFactory, paypalFactory, transferFactory).get();
    }
}
