package labs.camel.generator.model.paymentype.factory;

import com.github.javafaker.Faker;
import labs.camel.generator.model.paymentype.Card;
import labs.camel.generator.model.paymentype.PaymentData;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

@Component
@AllArgsConstructor
public class CardFactory implements Supplier<PaymentData> {

    private Faker faker;

    @Override
    public Card get() {
        return Card.builder()
                .cardNumber(faker.business().creditCardNumber())
                .cardType(faker.options().option("visa", "mastercard"))
                .expiry(expiryDate())
                .build();
    }

    private Date expiryDate() {
        return faker.date().between(faker.date().past(30, TimeUnit.DAYS), faker.date().future(10000, TimeUnit.DAYS));
    }
}
