package labs.camel.generator.model.paymentype;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Card implements PaymentData{

    @NotEmpty
    private String cardNumber;
    @NotEmpty
    private String cardType;
    @FutureOrPresent
    private Date expiry;

}
