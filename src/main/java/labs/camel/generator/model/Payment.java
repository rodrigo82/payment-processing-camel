package labs.camel.generator.model;

import labs.camel.generator.model.paymentype.PaymentData;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Payment {

    @NotEmpty
    private String invoiceId;
    @NotEmpty
    private String orderId;
    @PastOrPresent
    private Date date;
    @NotNull
    @Valid
    private PaymentData paymentData;
    @NotEmpty
    @Valid
    private List<Product> products;
    @PositiveOrZero
    private double tax;
    @PositiveOrZero
    private double shipping;

    public double getTotalValue(){
        double total = products.stream().mapToDouble(Product::getPrice).sum();
        return total + tax + shipping;
    }

}
