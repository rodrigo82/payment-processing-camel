package labs.camel.generator;

import labs.camel.RouteConfig;
import labs.camel.generator.model.factory.PaymentFactory;
import lombok.AllArgsConstructor;
import org.apache.camel.ProducerTemplate;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PaymentProducer implements Runnable{

    private ProducerTemplate producerTemplate;
    private PaymentFactory factory;

    @Override
    public void run() {
        while(true){
            producerTemplate.sendBody(RouteConfig.INPUT, factory.get());
            try {
                Thread.sleep(100l);
            } catch (InterruptedException e) {
            }
        }
    }
}
